<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    protected $table = 'tele_source';
    protected $fillable = [
    	'source_name',
    	'source_type',
    ];
    protected $hidden = [
    	'source_id',
    	'status',
    	'note',
    	'groupid',
    	'created_by',
    	'created_at',
    	'trashed_by',
    	'trashed_at',
    	'is_default_inbound',
    	'updated_by',
    	'updated_at',
    ];
}
