<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Campaign extends Model
{
	use HasApiTokens, Notifiable;

    protected $table = 'tele_campaign';
    protected $primaryKey = 'campaign_id';
    protected $fillable = [
    	'campaign_name',
    	'campaign_type',
    	
    ];
    protected $hidden = [
    	'start_date',
    	'end_date',
    	'campaign_id',
    	'groupid',
    	//'updated_at',
    	'created_at',
    	'created_by',
    	'campaign_action_status',
    	'campaign_type_call',
    	'campaign_status',
    	'campaign_type_id',
    	'campaign_prefix',
    	'campaign_prefix_mobi',
    	'campaign_prefix_vina',
    	'campaign_prefix_viettel',
    	'campaign_prefix_fixed',
    	'campaign_prefix_other',
    	'campaign_script_id',
    	'result_group_id',
    	'source_id',
    	'campaign_done_at',
    	'list_assign_agent',
    	'deltime',
    	'delby',
    	'is_result_default',
    	'is_default_inbound',
    ];
  //   protected $appends = [
		// 'result_group_name',
  //       'result_group_status',
  //      	'source_type'
  //   ];

    // public function campaignResultGroup()
    // {
    // 	return $this->belongsTo(Campaignresultgroup::class, 'result_group_id', 'result_group_id');
    // }

    public function source()
    {
    	return $this->belongsTo(Source::class, 'source_id', 'source_id');
    }

    public function campaignResultGroup()
    {
    	return $this->belongsTo(Campaignresultgroup::class, 'result_group_id', 'result_group_id');
    }


    // public function getResultGroupNameAttribute()
    // {
    // 	return $this->campaignResultGroup->result_group_name;
    // }

    // public function getSourceNameAttribute()
    // {
    // 	return $this->source->source_name;
    // }

    // public function getResultGroupStatusAttribute()
    // {
    // 	return $this->campaignResultGroup->result_group_status;
    // }

    // public function getSourceTypeAttribute()
    // {
    // 	if ($this->source_id) {
    // 		return $this->source->source_id;
    // 	}
    // 	return null;
    // }
}
