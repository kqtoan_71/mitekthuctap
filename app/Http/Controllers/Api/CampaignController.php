<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Campaign;
use App\Source;
use App\Campaignresultgroup;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\DB;
use Validator;

class CampaignController extends Controller
{
    public $successStatus = 200;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $campaignresultgroup = Campaignresultgroup::all();
        // $source = Source::all();
        // $campaign = Campaign::all();
        // $response = [
        //     'campaigns' => Campaign::all(),
        // ];
        // $response = Campaign::with('campaignresultgroup')->get();
        // $response = Campaign::with('source')->get();
        //$data = Campaign::with('result_group_id')->with('result_group_name', 'result_group_status')->get();
        $response = Campaign::with('source', 'campaignresultgroup')->get();
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //code...
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Campaign $campaign)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'campaign_name' => 'required',
            'campaign_type' => 'required',
        ]);
        // print_r($input); exit();
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $campaign->campaign_name = $input['campaign_name'];
        $campaign->campaign_type = $input['campaign_type'];
        $campaign->save();
        $success['campaign_name'] = $campaign->campaign_name;
        $success['campaign_type'] = $campaign->campaign_type;
        // $success['api_token'] = $campaign->createToken('CampaignName')->accessToken;
        return response()->json(['success' => 'Created campaign successfully'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $campaign = Campaign::find($id);
        $source = Source::all();
        $campaignresultgroup = Campaignresultgroup::all();
        if(is_null($campaign)){
            return response()->json(['Error' => 'Id '.$id.' not found'], 401);
        }
        $success['name'] = $campaign->campaign_name;
        $success['type'] = $campaign->campaign_type;
        $success['start_date'] = $campaign->start_date;
        $success['end_date'] = $campaign->end_date;
        $success['source_name'] = $campaign->source_name;
        $success['source_type'] = $campaign->source_type; 
        $success['result_group_name'] = $campaign->result_group_name;
        $success['result_group_status'] = $campaign->result_group_status;       
        return response()->json($success, 401);

        //---------------------------------------
        // $campaign = Campaign::find($id);
        // //print_r($campaign); exit();
        // if(is_null($campaign)){
        //     return response()->json(['error' => 'Not found'], 401);
        // }
        // $success['name'] = $campaign->campaign_name;
        // $success['type'] = $campaign->campaign_type;
        // $success['start_date'] = $campaign->start_date;
        // $success['end_date'] = $campaign->end_date;
        // $success['campaign_action_status'] = $campaign->campaign_action_status;
        // $success['campaign_status'] = $campaign->campaign_status;
        // $success['campaign_type_call'] = $campaign->campaign_type_call;
        // $success['list_assign_agent'] = $campaign->list_assign_agent;
        // $success['api_token'] = $campaign->createToken('CampaignName')->accessToken;
        // return response()->json($success, 401);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $campaign = Campaign::find($id);
        // if(is_null($campaign)){
        //     return response()->json(['Error' => 'Not found'], 401);
        // }
        // $success['name'] = $campaign->campaign_name;

        // return response()->json($success, 401);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Campaign $campaign, $id)
    {
        $campaign = Campaign::find($id);
        if (is_null($campaign)) {
            return response()->json(['error' => 'Id '. $id .' not found'], 401);
        }
        $input = $request->all();
        $validator = Validator::make($input, [
            // 'campaign_name' => 'required',
            'campaign_type' => 'required',
            'campaign_list' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        // $campaign->campaign_name = $input['campaign_name'];
        $campaign->campaign_type = $input['campaign_type'];
        $campaign->campaign_list = $input['campaign_list'];
        $campaign->save();
        $success['campaign_name'] = $campaign->campaign_name;
        $success['campaign_type'] = $campaign->campaign_type;
        $success['campaign_list'] = $campaign->campaign_list;
        $success['api_token'] = $campaign->createToken('CampaignName')->accessToken;
        // return response()->json(['success' => $campaign->campaign_name .' update successfully'], 200);
        return response()->json($success, 200);

        //-------------------------------
        // $input = $request->all();
        // $validator = Validator::make([
        //     'campaign_name' => 'required',
        //     'campaign_type' => 'required',
        //     'campaign_action_status' => 'required',
        //     'start_date' => 'required|date|after:yesterday',
        //     'end_date' => 'required|date|after:start_date',
        //     'campaign_status' => 'required',
        // ]);
        // if ($validator->fails()) {
        //     return response()->json(['error' => 'Fails']);
        // }
        // $campaign->campaign_name = $input['campaign_name'];
        // $success['api_token'] = $campaign->createToken('CampaignName')->accessToken;
        // return response()->json($success, 401);
        // $input = $request->all();
        // $campaign = Campaign::find($id);
        // $campaign->fill($input::all())->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Campaign $campaign, $id)
    {
        $campaign = Campaign::find($id);
        // // print_r($campaign); exit();
        if (is_null($campaign)) {
            return response()->json(['Error' => 'ID '.$id.' not found'], 401);
        }
        //$campaign = delete();
        Campaign::destroy($id);
        $success = $campaign->campaign_name;
        return response()->json(['Success' => $success.' deleted successfully'], 200);
    }
}
