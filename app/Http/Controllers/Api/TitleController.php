<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Title;
use Illuminate\Support\Facades\Auth;
use Validator;

class TitleController extends Controller
{
    public $successStatus = 200;
    public function createtitle(Request $request){
    	$validator = Validator::make($request->all(), [
    		// 'titleName' => 'required',
    		// 'status' => 'required' 
    	]);
    	if ($validator->fails()) {          
	       return response()->json(['error'=>$validator->errors()], 401);
	    }
	    $input = $request->all();
	    $title = Title::create();
	    $success['api_token'] =  $user->createToken('TitleName')->accessToken;
	    $success['name'] =  $user->name;
	    return response()->json(['success'=>$success], $this->successStatus);   
    }
}
