<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Resultcall;

class ApiController extends Controller
{
    public function detail(Request $request){
    $agentId = $request['agentId'];
    $fromDate = $request['fromDate'];
	$toDate = $request['toDate'];
    $name = new Resultcall;
    $name = $name->detail($agentId,$fromDate,$toDate);
    return response()->json($name,200);
}
    public function agent(Request $request){
		$agentId = $request['agentId'];
		$fromDate = $request['fromDate'];
		$toDate = $request['toDate'];
		$name= new Resultcall;

  		$name = $name->assign($agentId,$fromDate,$toDate);
 		return response()->json($name,200);
	}
    public function totalfinal(Request $request){
        $agentId = $request['agentId'];
        $fromDate = $request['fromDate'];
        $toDate = $request['toDate'];
        $name= new Resultcall;

        $name = $name->getcall($agentId,$fromDate,$toDate);
        return response()->json($name,200);
    }
    public function insert(Request $request){
        $insert = [
        'campaign_id' => $request['campaignId'],
        'mobiles' => $request['mobiles'],
        'note' => $request['note'],
        ];
        $name= new Resultcall;
        $id = $name->insert($insert);
         if($id){
            return response()->json(['code'=> 200, 'status'=>'success','message'=>'insert success']);
         }
         else{
            return response()->json(['code'=> 400, 'status'=>'error','message'=>'insert no success']);
         } 
    }

 public function check(Request $request){
       
        if (isset($request['mobiles'])){
            $where =[
                'mobiles' => $request['mobiles'],
            ];
            $name= new Resultcall;

            $user = $name->check($where);
            if(count($user)>0){
               
            }else{
                return response()->json(['code'=> 200, 'status'=>'error','message'=>'khong ton tai user']);
            }
  
        }else{
                return response()->json(['code'=> 400, 'status'=>'error','message'=>'mobiles khong duoc rong']);
            }
            return $user;
    }
}
