<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class AuthController extends Controller
{
    //
    public $successStatus = 200;
    public function register(Request $request){
		$validator = Validator::make($request->all(), [ 
	    	'name' => 'required',
	      	'email' => 'required|email',
	      	'password' => 'required',  
	      	'c_password' => 'required|same:password', 
	    ]); 
	    if ($validator->fails()) {          
	       return response()->json(['error'=>$validator->errors()], 401);
	    }  
	    $input = $request->all();  
	 	$input['password'] = bcrypt($input['password']);
	 	$user = User::create($input); 
	 	$success['api_token'] =  $user->createToken('AppName')->accessToken;
	 	$success['name'] =  $user->name;
	 	return response()->json(['success'=>$success], $this->successStatus); 
 	}

 	public function login(){ 
		if(Auth::attempt(['email' => request('email'), 'password' => request('password')]))
		{ 
			$user = Auth::user(); 
			$success['api_token'] =  $user->createToken('AppName')->accessToken; 
			return response()->json(['success' => $success], $this->successStatus); 
		} else{ 
		   	return response()->json(['error'=>'Unauthorised'], 401); 
		} 
	}

	public function getUser() {
		$user = Auth::user();
		return response()->json(['success' => $user], $this->successStatus); 
	}

	public function show($id = 0){
		$user = User::find($id);
		// print_r($user); exit();
		if(is_null($user)){
			return response()->json(['error' => 'Not found'], 401);
		}
		$success['name'] = $user->name;
		$success['email'] = $user->email;
		$success['api_token'] = $user->createToken('AppName')->accessToken;
		return response()->json($success, 401);
	}

	public function update(Request $request, $id){
		// $input = $request->all();
		// $validator =  Validator::make($input, [
		// 	'name' => 'required',
		// 	'email' => 'required',
		// ]);
		// if ($validator->fails()) {
		// 	return response()->json(['error' => 'error'], 401)
		// }
	}

	public function delete(User $user, $id){
		$user = User::find($id);
		if (is_null($user)) {
			# code...
			return response()->json(['error' => 'Not found'], 401);
		}
		$user->delete();
		$success = array();
		return response()->json(['success' => 'Delete successfully'], 200);
	}

	// public function update(User $user){
		
	// }
}
