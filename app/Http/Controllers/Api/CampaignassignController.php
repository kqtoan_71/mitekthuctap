<?php

namespace App\Http\Controllers\Api;

/*- Api final theo agent và khoảng thời gian: tên agent, tổng đã gọi, 
tổng gọi thành công, tổng thời gian nói chuyện, tổng final*/

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Campaignassign;
use App\Resultcall;
use App\Sourcedata;
use App\User;

class CampaignassignController extends Controller
{
    public $successStatus = 200; 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function agent(Request $request)
    {       
        $agentId = $request['agentId'];
        $fromDate = $request['fromDate'];
        $toDate = $request['toDate'];

        $success = new Campaignassign;
        $success = $success->finall($agentId, $fromDate, $toDate);
        return response()->json($success, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
