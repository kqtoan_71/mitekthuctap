<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaignresultgroup extends Model
{
    protected $table = 'tele_campaign_result_group';
    protected $fillable = [
    	'result_group_name',
    	'result_group_status',
    ];
    protected $hidden = [
    	'result_group_id',
    	'description',
    	'groupid',
    	'created_at',
    	'created_by',
    	'trashed_by',
    	'trashed_at',
    	'updated_at',
    	'updated_by',
    ];
}
