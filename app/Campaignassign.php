<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Campaignassign extends Model
{
    protected $table = 'tele_campaign_assign';

    public function finall($agentId, $fromDate, $fromDate){
    	$user = DB::table('tele_result_call as c')
    	->leftjoin('tele_source_data as d', 'c.source_data_id', '=', 'd.id')
    	->leftjoin('tele_campaign_assign as a', 'c.assign_id', '=', 'a.assign_id')
    	->leftjoin('users as u', 'u.id', '=', 'c.agent_id')
    	->selectRaw("c.agent_id, u.name, d.name, c.record_total_call, c.record_total_talktime, c.record_total_answered, c.record_total_answered, a.last_call_time")
    	->whereRaw("u.id = '$agentId' AND a.last_call_time >= '$fromDate' AND a.last_call_time <= '$toDate'")
    	->get();
    	return $user;
    }

}
