<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Resultcall extends Model
{
    protected $table = 'tele_result_call';
    /* Api thống kê chi tiết */
    public function detail($agentId, $fromDate, $toDate){ 
  $user = DB::table('tele_result_call as c' )
  ->leftjoin('tele_source_data as d' , 'c.source_data_id', '=', 'd.id')
  ->leftjoin('tele_campaign_assign as a', 'a.last_result_call_id', '=', 'c.call_id')
  ->leftjoin('tele_campaign_result as r', 'r.result_id', '=', 'c.campaign_result_id')
  ->leftjoin('users as u', 'u.id', '=', 'c.agent_id')
  ->selectRaw("a.last_call_time,c.agent_id,u.name,c.is_result_final,c.source_data_id,c.`campaign_result_id`,c.last_call_number,c.list_call_number,c.note,c.result_step,r.result_name,c.call_id,c.campaign_result_list_id,c.call_disposition,c.call_billsec,c.call_recfilename,c.appointment,c.call_info,c.recall_time,c.appointment,c.appointment_place")
  ->whereRaw("u.id = '$agentId' AND a.last_call_time BETWEEN '$fromDate' AND '$toDate'")
  ->get();
  
   return $user;
  }
      /* Api final theo agent và khoản thời gian*/
    public function assign($agentId, $fromDate, $toDate){
    	$user = DB::table('tele_campaign_assign as a')   
    	->leftjoin('tele_result_call as c', 'a.is_final_last_call_id', '=', 'c.call_id')
      ->leftjoin('tele_campaign as ca', 'ca.campaign_id', '=', 'a.campaign_id')
    	->leftjoin('users as u', 'u.id', '=', 'a.agent_id')
    	->selectRaw("ca.campaign_name,a.last_call_time,u.name,a.agent_id,a.assign_id,a.is_unsuccess,a.campaign_id,
        c.record_total_answered,c.record_total_talktime,a.is_called,a.is_final,a.is_final_last_call_id")
    	->whereRaw("u.id = '$agentId' AND a.last_call_time BETWEEN '$fromDate' AND '$toDate'")
      ->whereRaw("a.is_final_last_call_id IS NOT NULL")
      ->get();
    	return $user;
    }
    /* Api báo cáo theo kết quả final*/
     public function getcall($agentId, $fromDate, $toDate){
     $user = DB::table('tele_campaign_assign as a')   
      ->leftjoin('tele_result_call as c', 'a.assign_id', '=', 'c.assign_id')
      ->leftjoin('users as u', 'u.id', '=', 'a.agent_id')
      ->leftjoin('tele_campaign_result as r', 'r.result_id', '=', 'c.campaign_result_id')
      ->leftjoin('tele_campaign as ca', 'ca.campaign_id', '=', 'a.campaign_id')
      ->selectRaw("a.last_call_time,r.result_name,ca.campaign_name,u.name,c.last_call_number,c.list_call_number,c.is_result_final,a.last_recall_time,a.appointment_notes,a.current_step,a.current_step_max,c.appointment_place,a.dialtimes,a.agent_id,a.assign_id,a.is_final,a.is_final_last_call_id,a.appointment_final,a.appointment_times,a.appointment_created_time")
      ->whereRaw("u.id = '$agentId' AND a.last_call_time BETWEEN '$fromDate' AND '$toDate'")
      ->get();
      return $user;
    }
    /* Api Add tele_deny_list*/
     public function insert ($insert){
      return DB::table('tele_deny_list')->insert($insert);
     }
     /* Api check tele_deny_list */
     public function check($where){
       $user = DB::table('tele_deny_list')
       ->selectRaw('*')
       ->where($where)
       ->get();
       return $user;
     }
}
