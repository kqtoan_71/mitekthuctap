<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', 'Api\AuthController@login');
Route::post('register', 'Api\AuthController@register');
Route::get('showuser/{id?}', 'Api\AuthController@show');
Route::delete('delete/{id?}', 'Api\AuthController@delete');
Route::get('campaign', 'Api\CampaignController@index');
Route::get('showcampaign/{id?}', 'Api\CampaignController@show');
Route::post('store', 'Api\CampaignController@store');
Route::put('update/{id?}', 'Api\CampaignController@update');
Route::put('edit/{id?}', 'Api\CampaignController@edit');
Route::delete('destroy/{id?}', 'Api\CampaignController@destroy');
Route::get('agent', 'Api\CampaignassignController@agent');
Route::post('agent', 'Api\Apicontroller@agent');
Route::post('detail','Api\ApiController@detail');
Route::post('totalfinal','Api\ApiController@totalfinal');
Route::post('insert','Api\ApiController@insert');
Route::post('check','Api\ApiController@check');
//Route::post('campaign', 'Api\CampaignController@store');
// Route::post('createtitle', 'Api\TitleController@createtitle');
Route::group(['middleware' => 'auth:api'], function(){
	Route::post('getUser', 'Api\AuthController@getUser');
});


